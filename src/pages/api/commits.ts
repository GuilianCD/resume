import getAllCommits from '@/data/commits'
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	const commits = await getAllCommits();
  res.json(commits);
}
