
import {
	Box,
	Heading,
	Text,
	Tooltip,
	Button,
	Flex,
	Tag,
	TagLeftIcon,
	TagLabel,
	Center,
} from "@chakra-ui/react";

import { FaMapPin, FaUniversity  } from "react-icons/fa";


import Guilian from "public/photo.jpg";
import Me from "@/data/guilian";
import CvImg from "./image";


export default function AboutMe() {
	const Description = () => {
		return (
			<Center width="90%">
				<Box   marginTop="2em">
					
			</Box>
		</Center>
		);
	};


	const MeLink: React.FC<{text: string, url: string, hover: string, icon: React.ReactElement, [key: string]: any}> = ({text, url, icon, hover, ...rest}) => {
		return (
			<Tooltip label={hover} >
				<Button leftIcon={icon} borderRadius="10px" size={{base: "lg", md: "md"}} as="a" href={url} {...rest}>
					<Text>{text}</Text>
				</Button>
			</Tooltip>
		);
	}





	return (
		<Flex
		as="header"
		align="center"
		direction="column"
		alignItems="center"
		width="100%"
		>
			<Flex direction={{base: "column", md: "row"}} alignItems="center" justifyContent="center" width="100%" gap="1em">
				<Box width="fit-content" height="fit-content" borderRadius="100%" border="1px" borderColor="whiteAlpha.400" overflow="clip">
					<CvImg
					width="16em"
					src={Guilian}
					alt="Picture of Guilian Celin--Davanture (me)"
					unoptimized
					/>
				</Box>

				<Box>
					<Heading 
					as="h1"
					size={{base: "2xl", sm: "xl", lg: "2xl"}} 
					textAlign={{base: "center", md: "left"}}
					>Guilian</Heading>
					<Heading 
					size={{base: "xl", sm: "xl", md: "xl"}} 
					textAlign={{base: "center", md: "left"}}
					>Celin--Davanture</Heading>

					<Flex gap="0.5em" marginTop="0.5em">
						<Tag size="lg" colorScheme="catppuccin.teal" borderRadius="full">
							<TagLeftIcon as={FaMapPin}/>
							<TagLabel>Orsay</TagLabel>
						</Tag>
						<Tag size="lg" colorScheme="catppuccin.teal" variant="subtle" borderRadius="full">
							<TagLeftIcon as={FaUniversity}/>
							<TagLabel>Université Paris-Saclay</TagLabel>
						</Tag>
					</Flex>
				</Box>
			</Flex>

			<Flex direction="column" align="center" width="fit-content" marginTop="1em">
				<Text paddingX="0.5em">
				 {Me.description}
				</Text>

				<Flex wrap="wrap" marginTop="1em" direction={{base: "column", sm: "row"}} align="center" justify="center" gap="1em">
					{Me.myLinks.map((link, i) => <MeLink {...link} key={i}/>)}
				</Flex>
		</Flex>


	</Flex>
	);
}
