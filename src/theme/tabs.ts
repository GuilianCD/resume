import { tabsAnatomy } from '@chakra-ui/anatomy'
import { createMultiStyleConfigHelpers } from '@chakra-ui/react'
import { mode } from '@chakra-ui/theme-tools'

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(tabsAnatomy.keys)

const baseStyle = definePartsStyle((props) => {
	const { colorScheme: c } = props // extract colorScheme from component props

	return {
		tab: {
			_active: {
				backgroundColor: "inherit",
			}
		},
	};
})

export const tabsTheme = defineMultiStyleConfig({ baseStyle })

