

import { FaGithub, FaGitlab } from 'react-icons/fa';
import { BiMailSend } from "react-icons/bi";


const MAIL = "gcd-pro@42l.fr";

export interface GLink {
	icon: React.ReactElement;
	text: string;
	hover: string;
	url: string;
}

export interface GJob {
	title: string;
	description: string;
}

export interface GuilianProps {
	description: string;
	myLinks: GLink[];
	job?: GJob;
	email: string;
}

export default {
	email: MAIL,
	description: "Writing code since secondary school, my first experience with programming was writing text games using windows batch files. Since then, I haven't stopped writing code, and am currently in the second year of my 3-years computer science degree. I'm currently doing mostly React and Typescript.",
	myLinks: [
		{
			icon: <FaGitlab/>,
			text: "My Gitlab",
			hover: "See my code",
			url: "https://gitlab.com/guiliancd"
		},
		{
			icon: <FaGithub/>,
			text: "My Github",
			hover: "See my code",
			url: "https://github.com/guiliancd"
		},
		{
			icon: <BiMailSend/>,
			text: "Send me an e-mail",
			hover: "Click to send me an e-mail",
			url: "mailto:" + MAIL
		},
	],
	job: {
		title: "I'm looking for an internship !",
		description: "I'm currently looking for a 10-weeks-minimum internship, from April 24th to (at least) July 6th, extendable up to August. If you're interested in my profile, do let me know by sending me an e-mail !",
	}
};
