import {ReactElement} from "react";
import {IconType} from "react-icons";
import { FaCode } from "react-icons/fa";
import { SiJavascript, SiTypescript, SiRust, SiReact, SiGo, SiChakraui, SiHtml5, SiNextdotjs, SiExpress, SiHaskell, SiC, SiNodedotjs, SiJava, SiLinux, SiDocker, SiVim, SiPython, SiAndroid, SiKotlin, SiCplusplus, SiWikidata, SiPhp, SiGraphql, SiPostgresql, SiMysql } from "react-icons/si";

export interface TechType {
	name: string; 
	hover?: string; 
	url?: string;
	icon: IconType;
	color: string;
	colorScheme: string;
}

const COLOR_DEFAULT = "catppuccin.text";
const CSCHEME_DEFAULT = "catppuccin.lavender";

const allTechs: {[key: string]: TechType} = {
	webExt: {
		name: "web-ext",
		hover: "Web Extensions tool",
		icon: FaCode,
		url: "https://github.com/mozilla/web-ext",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	js: {
		name: "JS",
		hover: "Javascript",
		icon: SiJavascript,
		url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
		color: "#f7df1e",
		colorScheme: 'catppuccin.yellow'
	},
	ts: {
		name: "TS",
		hover: "Typescript",
		icon: SiTypescript,
		url: "https://www.typescriptlang.org/",
		color: "#2875c3",
		colorScheme: 'catppuccin.blue'
	},
	express: {
		name: "Express.js",
		hover: "Node.js library to create a web server",
		icon: SiExpress,
		url: "https://expressjs.com/",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	node: {
		name: "Node.js",
		hover: "Server-side javascript",
		icon: SiNodedotjs,
		url: "https://nodejs.org/",
		color: "#026e00",
		colorScheme: 'catppuccin.green',
	},
	go: {
		name: "Go",
		icon: SiGo,
		url: "https://go.dev/",
		color: '#69d8e3',
		colorScheme: "catppuccin.sky",
	},
	htmlCss: {
		name: "HTML/CSS",
		icon: SiHtml5,
		color: "#e44d26",
		colorScheme: 'catppuccin.peach',
	},
	react: {
		name: "React",
		icon: SiReact,
		url: "https://reactjs.org/",
		color: "#61dafb",
		colorScheme: 'catppuccin.sky',
	},
	rust: {
		name: "Rust",
		icon: SiRust,
		url: "https://www.rust-lang.org/",
		color: "black",
		colorScheme: 'catppuccin.peach'
	},
	wails: {
		name: "Wails",
		icon: FaCode,
		hover: "A go framework for creating web apps",
		url: "https://wails.io/",
		color: '#df0000',
		colorScheme: "catppuccin.red",
	},
	chakra: {
		name: "Chakra UI",
		icon: SiChakraui,
		url: "https://chakra-ui.com/",
		color: "#62c9cb",
		colorScheme: 'catppuccin.sky',
	},
	nextjs: {
		name: "Next.js",
		icon: SiNextdotjs,
		url: "https://nextjs.org/",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	haskell: {
		name: "Haskell",
		icon: SiHaskell,
		url: "https://www.haskell.org/",
		color: "#5e5086",
		colorScheme: 'catppuccin.mauve',
	},
	c: {
		name: "C",
		icon: SiC,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	java: {
		name: "Java",
		icon: SiJava,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	linux: {
		name: "Linux",
		icon: SiLinux,
		hover: "I daily-drive linux (currently on Kubuntu).",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	docker: {
		name: "Docker",
		icon: SiDocker,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	vim: {
		name: "Vim",
		icon: SiVim,
		hover: "I use (neo)vim as my primary editor",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	python: {
		name: "Python",
		icon: SiPython,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	tkinter: {
		name: "Tkinter",
		icon: SiPython,
		hover: "Python library to create GUIs",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	android: {
		name: "Android",
		icon: SiAndroid,
		color: '#3ddc84',
		colorScheme: 'catppuccin.green',
	},
	kotlin: {
		name: "Kotlin",
		icon: SiKotlin,
		color: '#7f52ff',
		colorScheme: 'catppuccin.sapphire',
	},
	cpp: {
		name: "C++",
		icon: SiCplusplus,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	semanticWeb: {
		name: "Structured data",
		icon: SiWikidata,
		hover: "This may be a little abstract, but I love data and relationships and organisation, and would love to learn more about data and how the web relies on it. ISOs and RFCs are my jam.",
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	php: {
		name: "PHP",
		icon: SiPhp,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	graphql: {
		name: "GraphQL",
		icon: SiGraphql,
		color: '#e632ad',
		colorScheme: 'catppuccin.pink',
	},
	postgresql: {
		name: "PostgreSQL",
		icon: SiPostgresql,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	},
	mysql: {
		name: "MySQL",
		icon: SiMysql,
		color: COLOR_DEFAULT,
		colorScheme: CSCHEME_DEFAULT,
	}
};



// techs I think I have an advanced level in
const techsIKnow = [
	allTechs.htmlCss,
	allTechs.nextjs,
	allTechs.js,
	allTechs.ts,
	allTechs.chakra,
	allTechs.node,
	allTechs.vim,
	allTechs.react,
	allTechs.linux,
];

// List of techs I've used, but wouldn't consider 
// having advanced knowledge in
const techsIUsed = [
	allTechs.go,
	allTechs.wails,
	allTechs.express,
	allTechs.webExt,
	allTechs.java,
	allTechs.docker,
	allTechs.cpp,
	allTechs.php,
	allTechs.mysql,
	allTechs.android,
	allTechs.c,
	allTechs.rust,
];

// Techs that I want to know more about
const techsIWantToLearn = [
	allTechs.rust,
	allTechs.c,
	allTechs.android,
	allTechs.haskell,
	allTechs.kotlin,
	allTechs.semanticWeb,
	allTechs.graphql,
	allTechs.postgresql,
];


export default {
	all: allTechs,
	know: techsIKnow,
	used: techsIUsed,
	learn: techsIWantToLearn
};
