import techs, {TechType} from "@/data/techs";
import {IconType} from "react-icons";
import {SiMozilla} from "react-icons/si";
import {TbWorld} from "react-icons/tb";


export interface ProjectType {
	title: string;
	repoUrl?: string;
	live?: {
		url: string;
		icon: IconType | null;
		colorScheme?: string;
	};
	techs: TechType[];
	description: string;
	shortDesc: string;
	uid: string;
}


const T = techs.all;



const allProjects: {[key: string]: ProjectType} = {
	mi9k: {
		title: "Moodle Improver 9000",
		repoUrl: "https://gitlab.com/improvingups/moodleimprover9000",
		live: {
			url: "https://addons.mozilla.org/en-US/firefox/addon/moodleimprover9000?utm_source=cv&utm_content=link",
			icon: SiMozilla,
			colorScheme: "catppuccin.teal"
		},
		techs: [T.webExt, T.js, T.htmlCss],
		shortDesc: "Web extension for my year group's moodle.",
		description: "My first ever attempt at an extension, it improved our year group's moodle. I was annoyed by how bad it was, so I improved it, adding shortcuts, an easy-to-access search bar, one-click-access to your current class, etc.",
		uid: "mi9k",
	},
	mycv: {
		title: "This resume",
		repoUrl: "https://gitlab.com/guiliancd/resume",
		live: {
			url: "https://guilian.dev",
			icon: null,
			colorScheme: "catppuccin.teal"
		},
		techs: [T.ts, T.react, T.chakra, T.nextjs],
		shortDesc: "My website-resume built using next.js",
		description: "I made this resume using Web Technologies™ mainly because I'm more at ease with code than a gui editor. I've always been at home with structured data and organisation, so writing it using html-like code was perfect. One key aspect of this resume is that I did a best-effort to make it easy ot change ; if I need to make small changes in the future (adding a project, removing the 'I'm looking for a job!' card, etc.), all I'll have to do is changes the corresponding 'data' file, which contain the presented data.",
		uid: "mycv",
	},

	nsiSudoku: {
		title: "Sudoku",
		repoUrl: "https://github.com/GuilianCD/nsi-sudoku",
		techs: [T.python, T.tkinter],
		shortDesc: "Desktop sudoku built with python",
		description: "As part of my high-school class of 'NSI' (comp-sci), we had to create a sudoku in a group of 3 using python and tkinter. One thing I am really proud about in this project is that I created my own 'scrollable frame' because Tkinter did not offer such a thing, for some reason. One of the first 'real' projects I worked on. I also did most of the work on that project.",
		uid: "nsi-sudoku",
	},
	notcodex: {
		title: "Meowmento",
		repoUrl: undefined,
		techs: [T.go, T.wails, T.ts, T.react, T.chakra],
		shortDesc: "Collaborative note taking app",
		description: "For our fourth semester's project at the Orsay IUT, me and 3 other students created a collaborative note-taking app that seeked to fill every issue we had with alternatives until then, and offer even more functionnalities. Main features : note taking in markdown, live collaboration with others.",
		uid: "codex",
	},
	firstYearImagesCpp: {
		title: "Image manipulation in C++",
		repoUrl: "https://github.com/GuilianCD/S101-projet",
		techs: [T.cpp],
		shortDesc: "Small program to manipulate ppm images.",
		description: "This school project had us in groups of two making a small C++ program that could manipulate ppm images.",
		uid: "year1cppimages",
	}
};

const myProjects = [
	allProjects.mi9k,
	allProjects.mycv,
];

const schoolProjects = [
	allProjects.notcodex,
	allProjects.nsiSudoku,
	allProjects.firstYearImagesCpp,
];

const currentProjects = [
	allProjects.notcodex,
	allProjects.mycv
];

const pinnedProjects = [
	allProjects.mi9k,
	allProjects.mycv
];



const categories = [
	{label: "Personal projects", all: myProjects},
	{label: "School projects", all: schoolProjects},
]
 
export default {
	all: allProjects,
	my: myProjects,
	school: schoolProjects,
	current: currentProjects,
	pinned: pinnedProjects,
	categories,
};
