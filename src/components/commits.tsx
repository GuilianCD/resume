
import {type allCommitsData} from "@/data/commits";

import {Box, Card, CardBody, CardHeader, Center, Divider, Flex, Heading, HStack, Link, Spacer, Spinner, Square, Stack, Stat, StatGroup, StatHelpText, StatLabel, StatNumber, Text, Tooltip, useBreakpointValue, VStack} from "@chakra-ui/react";

import dayjs from "dayjs";

import {ReactNode } from "react";
import useSWR from "swr";


function getISOFrom(daysAgo: number) {
	const day = dayjs().subtract(daysAgo, "days");
	return day.format("YYYY-MM-DD");
}


type CommitsColors = "github" | "gitlab" | "school";


const CommitOverlay = ({color, nb}: {color: CommitsColors, nb: number}) => {
	const bgColor = {github: "catppuccin.mauve", gitlab: "catppuccin.green", school: "catppuccin.sapphire"}[color];

	const values = () => {
		switch(true) {
			case nb > 20:
				return [1, 1];
			case nb > 10:
				return [3, 0.75];
			case nb > 5:
				return [5, 0.6];
			default:
				return [9 - 2 - nb, 0.5];
		}
	};
	const [val, opacity] = values();

	return (
		<Square position="absolute" size="100%" backgroundColor={bgColor + '.' + val + '00'} opacity={opacity}/>
	);
}

type CommitsBaseBoxProps = {boxSize: number, margin: number, borderRadius: string, label?: string, children: ReactNode, [key: string]: any};

function CommitsBaseBox ({boxSize, margin, borderRadius, children, label, ...rest}: CommitsBaseBoxProps) {

	const SurroundOpt = ({label, children}: {label?: string, children: ReactNode}) => {
		if(label !== undefined) {
			return (
				<Tooltip hasArrow label={label}>
					{children}
				</Tooltip>
			);
		}
		else {
			return (
				<>
					{children}
				</>
			);
		}
	};

	return (
		<SurroundOpt label={label}>
			<Square
			size={boxSize + "em"}
			margin={margin + 'em'}
			borderRadius={borderRadius}
			overflow="clip"
			backgroundColor="catppuccin.surface0"
			position="relative"
			{...rest}
			>
			{children}
		</Square>
	</SurroundOpt>
	);
}

function CommitsGraphColorsHelp({boxSize, margin, borderRadius, ...rest}: {boxSize: number, margin: number, borderRadius: string, [key: string]: any}) {

	const commitBreakpoints = [1, 6, 11, 21];


	const SingleHelp = ({color}:{color: CommitsColors}) => {
		return (
			<Flex>
				<Flex alignItems="center" marginRight="0.25em">
				{
					commitBreakpoints.map((val, i) => {
						return (
							<CommitsBaseBox
							boxSize={boxSize}
							margin={margin}
							borderRadius={borderRadius}
							key={i}
							>
								<CommitOverlay color={color} nb={val}/>
							</CommitsBaseBox>
						);
					})
				}
				</Flex>
				<Text>{color}</Text>
			</Flex>
		);
	};


	const colors: CommitsColors[] = ["github", "gitlab", "school"];

	return (
		<Stack direction={{base: "column", sm: "row"}} spacing={{base: "0", sm: "0.5em"}} alignItems="left" width="fit-content" {...rest}>
		{
			colors.map((c, i) => <SingleHelp color={c} key={i}/>)
		}
		</Stack>
	);
}


export default function CommitsGraph({mobile, day}: {mobile: boolean, day: string}) {
	const { data, error, isLoading } = useSWR('/api/commits', (...args) => fetch(...args).then(res => res.json()));

	// 0 is sunday, 6 is saturday
	// I HATE LOCALISATION
	const weekIndex = dayjs(day).day();

	const [boxSize, margin, borderRadius]: [number, number, string] = useBreakpointValue({
		base: [0.4, 0, "0px"],
		sm: [0.5, 0.05, "2px"],
		lg: [0.5, 1/16, "2px"]
	}, {fallback: mobile ? 'base' : 'lg'})!;



	let commits: allCommitsData;

	if(isLoading || error) {
		commits = {
			total: {
				all: 0,
				github: 0,
				gitlab: 0,
				iut: 0,
			},
			commits: {}
		};
	}
	else {
		commits = data as allCommitsData;
	}



	return (
		<Box paddingY={{base: 0, md: "1em"}} paddingBottom="0">
			<Center>
				<Box>
				{error || isLoading ?
					<Heading marginLeft="1em" marginBottom="0.25em" fontSize="x-large" as="h2" marginRight="0.25em" width="fit-content"><Spinner marginRight='1em'/>Loading contributions...</Heading>
					:
					<Heading marginLeft="1em" marginBottom="0.25em" fontSize="x-large" as="h2" marginRight="0.25em" width="fit-content">{commits.total.all} contributions in the last year</Heading>
				}

				<Flex
				wrap="wrap"
				direction="column"
				padding="0"
				width={(53 * boxSize) + (53 * 2 * margin) + "em"}
				height={(7 * boxSize) + (7 * 2 * margin) + "em"}
				align="flex-start"
				justify="flex-start"
				margin="1em"
				marginTop="0"
				>
				{
					(weekIndex != 0) && Array(weekIndex).fill(null).map((_, i) => {
						const day = getISOFrom(365 - 1  - i + weekIndex);
						const dateStr = dayjs(day).format("ddd. D, MMMM, YYYY");

						return (
							<CommitsBaseBox
							label={"No contributions on " + dateStr}
							boxSize={boxSize}
							margin={margin}
							borderRadius={borderRadius}
							key={i}
							>
							</CommitsBaseBox>
						);
					})
				}
				{
					Array(365).fill(null).map((_, i) => {
						const day = getISOFrom(365 - 1 - i);
						const data = commits.commits[day];

						const dateStr = dayjs(day).format("ddd. D, MMMM, YYYY");

						let label = "No contributions on " + dateStr;

						if(data !== undefined && (data.all > 0)){
							label = data.all + " contributions on " + dateStr;// + "(" + data.gitlab + " on gitlab, " + data.github + " on github)";
						}

						return (
							<CommitsBaseBox
							label={label}
							boxSize={boxSize}
							margin={margin}
							borderRadius={borderRadius}
							key={i}
							>
							{data && (data?.github > 0) && <CommitOverlay color="github" nb={data.github} />}
							{data && (data?.gitlab > 0) && <CommitOverlay color="gitlab" nb={data.gitlab} />}
							{data && (data?.iut > 0) && <CommitOverlay color="school" nb={data.iut} />}
						</CommitsBaseBox>
						);
					})
				}
				{
					(weekIndex != 6) && Array(6 - weekIndex).fill(null).map((_, i) => {
						const day = getISOFrom(365 - 1 - i);
						const dateStr = dayjs(day).format("ddd. D, MMMM, YYYY");

						return (
							<CommitsBaseBox
							label={"No contributions yet on " + dateStr}
							boxSize={boxSize}
							margin={margin}
							borderRadius={borderRadius}
							key={i}
							>
							</CommitsBaseBox>
						);
					})
				}
			</Flex>

			<Flex>
				<CommitsGraphColorsHelp boxSize={boxSize} margin={margin} borderRadius={borderRadius}/>

				<Text marginTop="0.5em" marginLeft="auto" textAlign="right" fontSize="small" color="whiteAlpha.600">Some commits might be missing. <Link style={{textDecoration: 'underline'}} href="https://gitlab.com/gitlab-org/gitlab-foss/-/issues/33486">Learn more</Link></Text>
			</Flex>
		</Box>
	</Center>

</Box>
	);
}
