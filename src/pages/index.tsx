import Head from 'next/head'


import {
	Box,
	Icon,
	Heading,
	Text,
	Button,
	Divider,
	VStack,
	Card,
	CardBody,
	CardHeader,
	SimpleGrid,
	Wrap,
	Center,
	WrapItem,
	Stack,
	Flex,
	useBreakpointValue,
	Tab,
	Tabs,
	TabList,
	TabPanel,
	TabPanels,
	CardFooter,
	Circle,
	HStack,
	chakra,
	Link,
	TabIndicator
} from "@chakra-ui/react";

import Project from "@/components/project";
import TechLabel from '@/components/techlabel';

import { TechType } from "@/data/techs";
import projects, { ProjectType } from '@/data/projects';
import techs from '@/data/techs';
import getAllCommits, {allCommitsData} from '@/data/commits';
import {GetServerSideProps} from 'next';
import CommitsGraph from '@/components/commits';
import AboutMe from '@/components/about';


import Me from "@/data/guilian";
import {BiMailSend} from 'react-icons/bi';
import {FaUniversity} from 'react-icons/fa';

import MacchiatoPalette from "public/macchiato.png";
import CvImg from '@/components/image';
import isMobile from 'is-mobile';
import {useRouter} from 'next/router';
import {useEffect, useState} from 'react';
import {SWRConfig} from 'swr';

import dayjs from "dayjs";

function CvBox({children, ...rest}: {children: React.ReactNode, [key: string]: any}) {
	return (
		<Box padding="1em" borderRadius={{base: 0, lg: "1em"}} backgroundColor="whiteAlpha.50" {...rest}>
		{children}
	</Box>
	);
}


function CategoryProjects({title, projects, ...rest}: {title: string, projects: ProjectType[], [key: string]: any}) {
	return (
		<Box width="100%"  {...rest} padding="1em" borderRadius={{base: 0, lg: "2em"}}>
			<Heading as="h3">{title} <Text as="span" color="whiteAlpha.300">({projects.length})</Text></Heading>

			<SimpleGrid columns={{base: 1, xl: 2, "2xl": 3}} spacing="1em" padding="0.25em" marginY="1em">
			{
				projects.map((project, i) => <Project key={i} project={project}/>)
			}
		</SimpleGrid>
	</Box>
	);
}

function AllMyProjects({...rest}: {[key: string]: any}) {
	return (
		<VStack {...rest}>
		{
			projects.categories.map((cat,i) => (
				<CvBox padding="1em" key={i}>
					<CategoryProjects title={cat.label} projects={cat.all}/>
				</CvBox>
			))
		}
	</VStack>
	);
}





function TechList({techs, title, desc, size, ...rest}:{techs: TechType[], size?: 'sm' | 'md' | 'lg', title: string, desc: string, [key: string]: any}) {
	return (
		<Card width="100%" variant="outline" {...rest}>
			<CardHeader paddingBottom="0">
				<Heading as="h3" fontSize="x-large">{title} :</Heading>
				<Text>{desc}</Text>
			</CardHeader>
			<CardBody>
				<Wrap justify="flex-start" spacing="5px">
				{techs.map((tech, i) => (
					<WrapItem key={i}>
						<TechLabel size={size} tech={tech}/>
					</WrapItem>
				))}
			</Wrap>
		</CardBody>
	</Card>
	);
}

function MyTechnologies({...rest}: {[key: string]: any}) {
	return (
		<Box height="100%" {...rest}>
			<Heading as="h2" fontSize="large" marginBottom="0.25em" fontWeight="bold">Technologies</Heading>
			<Stack direction={{base: 'column', lg: 'row'}} spacing="1em" padding="0.25em">
				<TechList size='md' flex={1} techs={techs.know} title="Experienced in" desc="I feel confident using these, and have used them quite a lot"/>
				<TechList size='md' flex={1} techs={techs.used} title="Used before" desc="I've used these before, but haven't quite mastered them yet"/>
				<TechList size='md' flex={1} techs={techs.learn} title="Interested to learn" desc="I would like to learn more about these ; this category can be interpreted as my 'where I want to go'"/>
			</Stack>
		</Box>
	);
}




function SexyList({projects, title, ...rest}: {projects: ProjectType[], title: string, [key: string]: any}){
	return (
		<Box width="100%" {...rest}>
			<Heading as="h2" fontSize="large" marginBottom="0.5em" fontWeight="bold">{title}</Heading>
			<Center width="100%">
				<SimpleGrid columns={{base: 1, lg: 2}} gap="0.5em" width="100%" maxWidth="120em">
				{projects.map((project, i) => <Project project={project} key={i}/>)}
			</SimpleGrid>
		</Center>
	</Box>
	);
}


const mainCss  = {
	topMargin: "3em",
	tabsHeight: "2em",
	border: {
		size: "2px",
		color: "catppuccin.surface0",
	},
	alignedMargin: "",
};
mainCss.alignedMargin = `calc(${mainCss.tabsHeight} - ${mainCss.border.size})`



export default function Home({mobile, day}: {mobile: boolean, day: string}) {
	const isSmallUi: boolean = useBreakpointValue({
		base: true,
		md: false
	}, {fallback: mobile ? "base" : "md"})!;



	return (
		<>
			<Head>
				<title>Guilian Celin--Davanture</title>
				<meta name="description" content="Curriculum Vitae de Guilian Celin--Davanture" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<Flex
			marginTop={{base: 0, md: mainCss.topMargin }}
			width="100%"
			justifyContent="center"
			position="relative"
			height="calc(fit-content - 3rem)"
			>

			{true && (
				<Box
				position="absolute"
				width="100%"
				height="5px"
				top="0"
				left="0"
				zIndex="-1"
				marginTop={mainCss.alignedMargin}
				borderTop={mainCss.border.size}
				borderColor={mainCss.border.color}
				/>
			)}

			<Stack
			direction={{base: "column", md: "row"}}
			gap="0"
			spacing="0"
			width="100%"
			maxWidth="100em"
			justifyContent={{base: undefined, md: "center"}}
			>

			<Box
			marginTop={mainCss.alignedMargin}
			paddingTop="1em"
			borderTop={mainCss.border.size}
			borderColor={mainCss.border.color}
			flex="1"

			>
				<VStack
				padding={{base: "0.3em", lg: "0.5em"}}
				height="fit-content"
				width="fit-content"
				maxWidth="100vw"
				transform={{base: undefined, lg: "translateY(-5em)"}}
				marginBottom={{base: "2em", lg: undefined}}
				divider={<Divider orientation='horizontal' marginY="1em"/>}
				>
					<AboutMe/>

					<CommitsGraph mobile={mobile} day={day}/>

					{Me.job !== undefined && (
						<>
							<Center width="min(36em, 90%)">
								<Card align='center'>
									<CardHeader>
										<Heading size='md'>{Me.job.title}</Heading>
									</CardHeader>
									<CardBody>
										<Text>{Me.job.description}</Text>
									</CardBody>
									<CardFooter>
										<Button
										variant="solid"
										colorScheme='catppuccin.blue'
										leftIcon={<BiMailSend/>}
										borderRadius="10px" size={{base: "lg", md: "md"}}
										as="a"
										href={"mailto:" + Me.email}
										>
											<Text>Send me an e-mail</Text>
										</Button>
									</CardFooter>
								</Card>
							</Center>
						</>
					)}
				</VStack>
			</Box>


			<Tabs height="min-content" width="100%" flex="2" isFitted={isSmallUi} colorScheme="catppuccin.sky">
				<TabList height={mainCss.tabsHeight} borderColor={mainCss.border.color} borderBottom={{base: mainCss.border.size, md: 0}}>
					<Tab>Overview</Tab>
					<Tab>Projects</Tab>
				</TabList>


				<TabPanels paddingTop="1em">
					<TabPanel>
						<Flex direction="column" gap="2em">
							<SexyList projects={projects.pinned} title="Pinned"/>
							<MyTechnologies/>
						</Flex>
					</TabPanel>

					<TabPanel>
						<Flex direction="column" gap="2em">
							<SexyList projects={projects.current} title="Current projects"/>
							<SexyList projects={[...projects.my, ...projects.school]} title="All my projects"/>
						</Flex>
					</TabPanel>
				</TabPanels>
			</Tabs>
		</Stack>

	</Flex>

	{/*<Text width="fit-content">Made with <Link href="https://nextjs.org" color="catppuccin.red.100">Next.js</Link>, <Link href="https://netlify.com" color="catppuccin.green.100">Netlify</Link>, and the <Link href="https://github.com/catppuccin" color="catppuccin.sapphire.100">catppuccin color palette</Link>.</Text>*/}
		{/*<Flex
			alignItems="center"
			direction="column"
			as="footer"
			width="100%"
			backgroundColor="catppuccin.crust"
			padding="1em"
			>
			</><CvImg
			width="60%"
			src={MacchiatoPalette}
			alt="Colors from the Catppuccin Macchiato palette"
			/>
	</Flex>*/}

</>
	)
}




export async function getServerSideProps<GetServerSideProps>(context: any) {

	const mobile = isMobile({ua: context.req.headers['user-agent']});

	// wow I sure do love localisation-related bugs
	// I hope I get more !!!!!
	// I have been a good bing
	const day = dayjs().format("YYYY-MM-DD");

	// I think these are good values,
	// it should always be almost up to date.
	//
	// stale :	10 minutes (10 * 60)
	// SWR :		30 minutes (30 * 60)
	context.res.setHeader(
		'Cache-Control',
		'public, s-maxage=600, stale-while-revalidate=1800'
	)


return {
	props: {
		mobile: mobile,
		day: day
	},
}
}

