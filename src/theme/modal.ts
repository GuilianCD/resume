import { modalAnatomy as parts } from '@chakra-ui/anatomy'
import { createMultiStyleConfigHelpers } from '@chakra-ui/styled-system'

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(parts.keys)

const baseStyle = definePartsStyle({
  // define the part you're going to style
  overlay: {
    bg: 'blackAlpha.700', //change the background
  },
  dialog: {
    borderRadius: 'md',
    bg: `catppuccin.base`,
		_dark: {
			bg: `catppuccin.base`,
			color: 'catppuccin.text'
		}
  },
})

export const modalTheme = defineMultiStyleConfig({
  baseStyle,
})

