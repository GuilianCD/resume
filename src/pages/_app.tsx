import '@/styles/globals.css'
import type { AppProps } from 'next/app'

import { ChakraProvider, ColorModeScript } from '@chakra-ui/react'
import config from "@/theme";



export default function App({ Component, pageProps }: AppProps) {
	return (
		<ChakraProvider theme={config}>
			<ColorModeScript initialColorMode='dark'/>
			<Component {...pageProps} />
		</ChakraProvider>
	);
}
