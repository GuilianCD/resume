import {
	Text,
	Tooltip,
	Button,
	Flex,
	Box,
	Icon
} from "@chakra-ui/react";

import { FaCode } from "react-icons/fa";

import { TechType } from "@/data/techs";
import {useState} from "react";

export default function TechLabel({tech, size, ...rest}: {tech: TechType, size?: 'sm' | 'md' | 'lg', [key:string]: any}) {
	return (
		<Tooltip label={tech?.hover || tech.name} >
			<Button 
			size={size ?? 'sm'} 
			leftIcon={<Icon as={tech.icon}/>} 
			borderRadius="full" 
			{...(tech?.color && {color: tech.color})}
			{...(tech?.url && {as:"a", href: tech.url})} 
			{...rest}
			>
				<Text fontSize="small" color="catppuccin.text">{tech.name}</Text>
			</Button>
		</Tooltip>
	);
}

export function SleekTechLabel({tech, size, ...rest}: {tech: TechType, size?: 'sm' | 'md' | 'lg', [key:string]: any}) {
	const [hovered, setHovered] = useState(false);

	return (
		<Tooltip label={tech?.hover || tech.name} >
			<Flex
			{...(tech?.url && {as:"a", href: tech.url})}
			{...(tech?.color && {color: tech.color})}

			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
			{...(hovered && {backgroundColor:"catppuccin.surface0"})}
			
			paddingX="0.75em"
			paddingY="0.5em"
			borderRadius="1em"
			alignItems="center"
			{...rest}
			>
				<Icon as={tech.icon}/>
				<Text marginLeft="0.5em" fontSize="small" fontWeight="light" color="catppuccin.subtext1">{tech.name}</Text>
			</Flex>
		</Tooltip>
	);
}

export function IconOnlyTechLabel({tech, size, ...rest}: {tech: TechType, size?: 'sm' | 'md' | 'lg', [key:string]: any}) {
	const techIcon = tech?.icon || <FaCode/>;

	return (
		<Tooltip label={tech?.hover || tech.name} >
			<Box
			padding="5px"
			{...(tech?.url && {as:"a", href: tech.url})}
			{...(tech?.color && {color: tech.color})}
			{...rest}
			>
				<Icon as={tech.icon}/>
			</Box>
		</Tooltip>
	);
}
