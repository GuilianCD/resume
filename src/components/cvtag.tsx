
import {
	Box,
	Text,
	Tooltip,
	Button,
	BoxProps,
} from "@chakra-ui/react";

interface ToolippedTagProps extends BoxProps {
	icon: React.ReactElement;
	tooltip: string;
	text: string;
	link?: string;
	[key: string]: any;
}

const TooltipTag: React.FC<ToolippedTagProps> = ({tooltip, text, icon, link, ...rest}) => {
	return (
		<Box {...rest}>
			<Tooltip label={tooltip}>
				<Button variant="solid" leftIcon={icon} as="a" {...(link && {href: link})}>
					<Text>{text}</Text>
				</Button>
			</Tooltip>
		</Box>
	);
}

export default TooltipTag;
