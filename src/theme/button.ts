
import { defineStyle, defineStyleConfig } from '@chakra-ui/react'


// Defining a custom variant
const catppuccin = defineStyle((props) => {
  return {
    bg: `catppuccin.surface0`,
    _dark: {
      bg: `catppuccin.surface0`,
    },

    _hover: {
      bg: `catppuccin.surface1`,
			_dark: {
				bg: `catppuccin.surface1`,
			}
    },

    _active: {
      bg: `catppuccin.surface2`,
			_dark: {
				bg: `catppuccin.surface2`,
			}
    },
  }
})




export const buttonTheme = defineStyleConfig({
  variants: { catppuccin: catppuccin },
	defaultProps: {
    variant: 'catppuccin',
  },
})
