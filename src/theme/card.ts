
import { cardAnatomy } from '@chakra-ui/anatomy'
import { createMultiStyleConfigHelpers } from '@chakra-ui/react'

const { definePartsStyle, defineMultiStyleConfig } = createMultiStyleConfigHelpers(cardAnatomy.keys)


const baseStyle = definePartsStyle({
  // define the part you're going to style
  container: {
    backgroundColor: "catppuccin.surface0",
		color: "catppuccin.text",
  },
  header: {
    paddingBottom: "0.5em"
  },
  body: {
    paddingTop: "1em"
  },
  footer: {
    paddingTop: "1em"
  }
});

// define custom variant
const variants = {
  outline: definePartsStyle({
    container: {
			borderColor: "catppuccin.surface0",
			backgroundColor: "catppuccin.base",
      borderWidth: "2px",
      color: "catppuccin.text",
			_dark: {
				borderColor: "catppuccin.surface0",
				backgroundColor: "catppuccin.base",
			}
    }
  })
};


export const cardTheme = defineMultiStyleConfig({
	baseStyle,
	variants
})
