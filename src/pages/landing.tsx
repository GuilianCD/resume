import {VStack, Center, Heading, Link, Flex, Box, Card, CardHeader, CardBody, Text, SimpleGrid} from "@chakra-ui/react";
import {ReactNode, useState} from "react";
import NextLink from 'next/link'




export default function Landing() {
	return (
		<Center width="100%">
			<VStack maxWidth={{base: '100%', lg: '33.3vw'}} width="100%">
				<Heading><Link href="https://guilian.dev">guilian.dev</Link></Heading>

				<Box height="2em"/>

				<VStack width="100%">
					<CatItem title="Mon CV" desc="Why not ?" page="/cv"/>
				</VStack>

				<VStack width="100%">
					<Heading>{"Other stuff"}</Heading>
					<SimpleGrid columns={{base: 1, md: 2}} spacing='1em' width="100%">
						<CatItem title="CarboneIUT" desc="Why not ?" page="/carboneiut"/>
					</SimpleGrid>
				</VStack>
			</VStack>
		</Center>
	);
}



function CatItem({title, desc, page, shallow}: {desc: string, title: string, page: string, shallow?: boolean}) {
	const [hovered, setHovered] = useState(false);

	return (
		<Card 
			as={NextLink}
			shallow={shallow ?? true}
			href={page} 
			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
			variant={hovered ? "outline" : "elevated"}
			borderColor={hovered ? "inherit" : "transparent"}
			borderWidth="2px"
			borderRadius="1em"
			width="100%"
			>
			<CardHeader>
				<Heading style={{textDecoration: 'none'}}>{title}</Heading>
			</CardHeader>
			<CardBody>
				<Text style={{textDecoration: 'none'}}>{desc}</Text>
			</CardBody>
		</Card>
	);
}
