import { defineStyle, defineStyleConfig } from '@chakra-ui/react'

const brandPrimary = defineStyle({
    borderColor: 'catppuccin.surface0',
    _dark: {
        borderColor: 'catppuccin.surface0',
    }
})

export const dividerTheme = defineStyleConfig({
    variants: { solid: brandPrimary },
})
