import {Box, Button, Center, Flex, Heading, HStack, Input, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Radio, RadioGroup, Stack, Tab, TabList, TabPanel, TabPanels, Tabs, Text, useMultiStyleConfig, useTab, VStack} from "@chakra-ui/react"
import React, {ReactNode, Ref, useState} from "react"

type minmax = [number, number];

interface DATA_Type {
	transport: {
		[key: string]: number | minmax
	},
	meal: {
		[key: string]: number
	},
	computer: {
		[key: string]: number
	}
};

const DATA: DATA_Type = {
	meal: {
		vegan: 0.4,
		veggie: 0.5,
		beef: 7,
		other: 1.1
	},
	computer: {
		own: 2.5,
		iut: 5,
	},
	transport: {
		car: [4.5, 20],
		cocar: [2, 10],
		bike: [0.001, 0.22],
		public: [0.2, 1],
		other: [0, 20]
	}
}



export default function CarboneIUT() {
	const [co2, setCo2] = useState<[number, number][]>([[0,0]]);
	const [tabIndex, setTabIndex] = useState(0);


	const nextTab = () => setTabIndex(tabIndex + 1);

	const handleTabsChange = (index: number) => {
		setTabIndex(index)
	}



	const addCo2 = (arg:  number | minmax)=> {
		setCo2([...co2, as2(arg)]);
		nextTab();
	}

	const as2 = (arg: number | minmax): minmax => {
		if(Array.isArray(arg)) {
			return arg;
		}
		else {
			return [arg, arg];
		}
	}


	return (
		<>
			<Tabs index={tabIndex} onChange={handleTabsChange}>
				<TabList>
					<Tab isDisabled>Bienvenue</Tab>
					<Tab isDisabled>Transports</Tab>
					<Tab isDisabled>Repas</Tab>
					<Tab isDisabled>Ordinateur</Tab>
					<Tab isDisabled>Bilan</Tab>
				</TabList>
				<TabPanels>
					<TabPanel>
						<Center>
							<VStack>
								<Heading>Bienvenue !</Heading>
								<Button onClick={nextTab}>Continuer</Button>
							</VStack>
						</Center>
					</TabPanel>
					<TabPanel>
						<Center>
							<VStack>
								<Heading>Transport pour venir à l{"'"}iut</Heading>
								<Flex>
									<Button onClick={() => addCo2(DATA.transport.car)}>Voiture</Button>
									<Button onClick={() => addCo2(DATA.transport.cocar)}>Covoiturage</Button>
									<Button onClick={() => addCo2(DATA.transport.bike)}>Vélo</Button>
									<Button onClick={() => addCo2(DATA.transport.public)}>Transports en commun</Button>
									<Button onClick={() => addCo2(DATA.transport.other)}>Autre</Button>
								</Flex>
							</VStack>
						</Center>
					</TabPanel>
					<TabPanel>
						<RepasInput onNext={(n) => addCo2(n)}/>
					</TabPanel>
					<TabPanel>
						<MachineInput onNext={(n) => addCo2(n)}/>
					</TabPanel>
					<TabPanel>
						<Center>
							<VStack>
								<Heading>Vous émettez entre {co2.reduce(([pmin, pmax], [min, max]) => [pmin + min, pmax+max]).map(e => e + 'kg ').join(' et ')} de CO2 par semaine.</Heading>
								<Button onClick={() => {setCo2([[0,0]]); setTabIndex(0)}}>Recommencer</Button>
							</VStack>
						</Center>
					</TabPanel>
				</TabPanels>
			</Tabs>
		</>
	);
}

function RepasInput({onNext}:{onNext: (n: number) => void}) {
	const [vegan, setVegan] = React.useState(0);
	const [veggie, setVeggie] = React.useState(0);
	const [beef, setBeef] = React.useState(0);
	const [other, setOther] = React.useState(0);

	const handleNext = () => {
		const result = (vegan * DATA.meal.vegan) + (veggie * DATA.meal.veggie) +(beef * DATA.meal.beef) + (other * DATA.meal.other);
		onNext(result);
	}

	const SingleRepas = ({data, setData, label}: {label:string, data: number, setData: any}) => {
		const [value, setValue] = React.useState(0)

		return (
			<VStack>
				<Text>{label}</Text>
				<NumberInput
				onChange={(valueString, valueNumber) => setValue(valueNumber)}
				value={value}
				max={28}
				min={0}
				>
					<NumberInputField />
					<NumberInputStepper>
						<NumberIncrementStepper />
						<NumberDecrementStepper />
					</NumberInputStepper>
				</NumberInput>
			</VStack>
		)
	}

	return (
		<Center>
			<VStack>
				<Heading>Repas</Heading>
				<Flex>
					<SingleRepas data={vegan} setData={setVegan} label="Vegan"/>
					<SingleRepas data={veggie} setData={setVeggie} label="Vegétarien"/>
					<SingleRepas data={beef} setData={setBeef} label="Boeuf"/>
					<SingleRepas data={other} setData={setOther} label="Autre"/>
				</Flex>
				<Button onClick={handleNext}>Continuer</Button>
			</VStack>
		</Center>
	);
}

function MachineInput({onNext}:{onNext: (n: number) => void}) {
	const [value, setValue] = React.useState<string>('own');

	const handleNext = () => {
		onNext(value === "own" ? DATA.computer.own : DATA.computer.iut);
	}

	return (
		<Center>
			<VStack>
				<Heading>Repas</Heading>
				<RadioGroup onChange={(v) => setValue(v)} value={value}>
					<Stack direction='row'>
						<Radio value='own'>PC personnel</Radio>
						<Radio value='iut'>machine de l{"'"}iut</Radio>
					</Stack>
				</RadioGroup>
				<Button onClick={handleNext}>Continuer</Button>
			</VStack>
		</Center>
	)
}


