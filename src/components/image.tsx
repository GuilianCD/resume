
import {chakra} from '@chakra-ui/react';
import Image from 'next/image'

const CvImg = chakra(Image, {
	shouldForwardProp: (prop) => ['height', 'width', 'quality', 'src', 'alt', 'unoptimized'].includes(prop)
});

export default CvImg;
