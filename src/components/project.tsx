
import {
	Heading,
	Text,
	Tooltip,
	Button,
	Divider,
	Card,
	CardBody,
	CardHeader,
	CardFooter,
	Wrap,
	Flex,
	Tag,
	TagLeftIcon,
	TagLabel,
	Modal,
	useDisclosure,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalCloseButton,
	ModalBody,
	ModalFooter,
	Link,
	Box
} from "@chakra-ui/react";

import { FaGitlab, FaGithub, FaCode, FaRegSadTear } from "react-icons/fa";
import { BsTextParagraph, BsQuestionDiamond } from "react-icons/bs";

import TechLabel, {SleekTechLabel, IconOnlyTechLabel} from "@/components/techlabel";
import { ProjectType } from "@/data/projects";
import {useContext, useEffect, useState} from "react";

import styles from "@/styles/Home.module.css"

function RepoTags({repo}: {repo?: string}) {
	if(repo === undefined) {
		return (
			<Tooltip label="The code is not publicly available">
				<Tag>
					<TagLabel>private</TagLabel>
				</Tag>
			</Tooltip>
		);
	}

	if(repo.includes("github")) {
		return (
			<>
				<Tooltip label="The code is publicly available">
					<Tag>
						<TagLabel>public</TagLabel>
					</Tag>
				</Tooltip>

				<Tooltip label="Available on Github">
					<Tag>
						<TagLeftIcon as={FaGithub}/>
						<TagLabel>github</TagLabel>
					</Tag>
				</Tooltip>
			</>
		);
	}
	if(repo.includes("gitlab")) {
		return (
			<>
				<Tooltip label="The code is publicly available">
					<Tag>
						<TagLabel>public</TagLabel>
					</Tag>
				</Tooltip>
				<Tooltip label="Available on Gitlab">
					<Tag>
						<TagLeftIcon as={FaGitlab}/>
						<TagLabel>gitlab</TagLabel>
					</Tag>
				</Tooltip>
			</>
		);
	}

	return (
		<Tag>
			<TagLeftIcon as={FaCode}/>
			<TagLabel>public</TagLabel>
		</Tag>
	);
}

export function ProjectRepoButton({repoUrl}: {repoUrl?: string}) {
	const chooseRepoLabel = (repo: string): {label: string, icon: React.ReactElement} => {
		if(repo.includes("github")) {
			return {label: "See it on Github", icon: <FaGithub/>};
		}
		if(repo.includes("gitlab")) {
			return {label: "See it on Gitlab", icon: <FaGitlab/>};
		}

		return {label: "See the code", icon: <FaCode/>};
	}
	if(repoUrl !== undefined) {
		const {label, icon} = chooseRepoLabel(repoUrl);

		return (
			<Tooltip label={label}>
				<Button leftIcon={icon} as="a" href={repoUrl}>
					<Text>See the repo</Text>
				</Button>
			</Tooltip>
		);
	}
	else {
		return (
			<Tooltip label="That project's repo is either gone or not public.">
				<Button leftIcon={<FaRegSadTear/>} isDisabled>
					<Text>No code repo</Text>
				</Button>
			</Tooltip>
		);
	}


}



export default function Project({project}: {project: ProjectType}) {
	const { isOpen, onOpen, onClose } = useDisclosure()

	const title = (project.repoUrl === undefined) ? project.title : <Link href={project.repoUrl}>{project.title}</Link>; 

	return (
		<>
			<Card width="100%" borderRadius="xl" variant="outline" paddingX={0} id={project.uid}>
				<CardHeader paddingBottom="0">
					<Flex wrap="wrap" gap="0.5em">
						<Heading as="h3" fontSize="larger" color="catppuccin.rosewater.100">{title}</Heading>
						<RepoTags repo={project.repoUrl}/>
					</Flex>
					
				</CardHeader>


				<CardBody>
					<Text fontSize="medium" >{project.shortDesc}</Text>
					<Button
					size="sm"
					height="min-content"
					padding="4px"
					leftIcon={<BsTextParagraph/>}
					onClick={onOpen}
					>
						<Text>full description</Text>
					</Button>
				</CardBody>

				<Divider/>

				<CardFooter padding="0.5em">
					<Wrap spacing="0em">{project.techs.map((tech, i) => <SleekTechLabel tech={tech} key={i}/>)}</Wrap>
				</CardFooter>
			</Card>

			<Modal isOpen={isOpen} onClose={onClose} preserveScrollBarGap={false}>
				<ModalOverlay/>
				<ModalContent>
					<ModalHeader>{project.title}</ModalHeader>
					<ModalCloseButton/>
					<ModalBody>
						<Text>{project.description}</Text>
						<Divider orientation="horizontal" marginY="1em"/>
						<Box>
							<Text marginBottom="0.75em">This project uses :</Text>
							<Wrap spacing="0.5em">{project.techs.map((tech, i) => <TechLabel tech={tech} key={i}/>)}</Wrap>
						</Box>
					</ModalBody>
					<ModalFooter>
						<Button onClick={onClose}>
							Close
						</Button>
					</ModalFooter>
				</ModalContent>
				
			</Modal>
		</>
	);
}


