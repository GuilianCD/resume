import AboutMe from "@/components/about";
import CommitsGraph from "@/components/commits";
import {SleekTechLabel} from "@/components/techlabel";
import allProjects, {ProjectType} from "@/data/projects";
import {TechType} from "@/data/techs";
import {Icon, Card, CardBody, CardHeader, Center, Heading, HStack, Tag, TagLabel, TagLeftIcon, Text, Tooltip, Flex, Box, Spacer, VStack, Button, ButtonGroup, IconButton, As, CardFooter, useBreakpointValue, Tabs, TabList, TabPanels, TabPanel, Tab, Divider, useColorMode, Square, TabIndicator, Grid, GridItem} from "@chakra-ui/react";
import isMobile from "is-mobile";
import {IconType} from "react-icons";
import {BiChevronRight, BiMailSend} from "react-icons/bi";
import {FaCode, FaGithub, FaGitlab, FaLock, FaMoon, FaPlus, FaSun} from "react-icons/fa";

import Me from "@/data/guilian";

import dayjs from "dayjs";
import React, {ReactNode, useContext, useEffect, useRef, useState} from "react";





function BottomLine({colors, ...rest}: {colors: string[], [key: string]: any}) {
	return (
		<Flex width="100%">
		{colors.map((color,i) => <Box backgroundColor={color} key={i} flex={1} {...rest}/>)}
	</Flex>
	);
}

/**
	* Returns :
	* - is the repo private
* - the name of the hosting company (github, gitlab)
* - the color associated with the company
* - the icon of the company
*/
function Coderepo(from: string | undefined): [boolean, string, string, IconType] {
	if(from === undefined) {
		return [true, "Private", "catppuccin.crust", FaLock];
	}
	if(from.toLowerCase().includes("github")) {
		return [false, "Github", "catppuccin.mantle", FaGithub];
	}
	if(from.toLowerCase().includes("gitlab")) {
		return [false, "Gitlab", "catppuccin.peach.500", FaGitlab];
	}
	return [false, "Repo", "catppuccin.base", FaCode];
}


function SingleTech ({tech}: {tech: TechType}) {
	return (
		<Tooltip label={tech?.hover || tech.name} >
			<Tag
			{...(tech?.url && {as:"a", href: tech.url})}
			variant="subtle"
			size="lg"
			colorScheme={tech?.colorScheme || "catppuccin.sky"}
			borderRadius="full"
			transition="all 0.1s ease"
			borderWidth="2px"
			borderColor="transparent"
			_hover={{borderColor: tech.colorScheme + '.100'}}
			>
				<TagLeftIcon as={tech.icon}/>
				<TagLabel>{tech.name}</TagLabel>
			</Tag>
		</Tooltip>
	)
};


function ProjectRepoButton({ url }: { url: string | undefined}) {
	const [isPrivate, host, color, repo] = Coderepo(url);


	return (
		<Button
		variant="outline"
		borderColor="catppuccin.text"
		color="catppuccin.text"
		_hover={{backgroundColor: "catppuccin.text", color: "catppuccin.base"}}
		borderRadius="full"
		boxShadow="2xl"
		as="a"
		href={url}
		leftIcon={<Icon as={repo} color={color} boxSize="5"/>}
		rightIcon={<Icon as={BiChevronRight} boxSize="6"/>}
		paddingRight="0.5em"
		isDisabled={isPrivate}
		>
			<Text>{host}</Text>
		</Button>
	);
}

function ProjectActionButton({ url, text, icon, colorScheme }: { url: string, text: string, icon: IconType | null, colorScheme: string }) {
	return (
		<ButtonGroup isAttached>
			<Button
			variant="solid"
			colorScheme={colorScheme}
			borderRadius="full"
			boxShadow="2xl"
			as="a"
			href={url}
			{...(icon !== null && {leftIcon: <Icon as={icon!} marginRight="0" boxSize="5"/>})}
			rightIcon={<Icon as={BiChevronRight} boxSize="6"/>}
			paddingRight="0.5em"
			>
				<Text>{text}</Text>
			</Button>
		</ButtonGroup>
	);
}


function NewProject({project}: {project: ProjectType}) {
	/*const [ highlighted, setHighlighted ] = useState(false);
	const { selectedProject, setSelectedProject } = useContext(selectedProjectContext);

	useEffect(() => {
		if ( selectedProject === project.uid && highlighted === false ) {
			setHighlighted( true );

			const element = document.querySelector("#" + selectedProject);
			element?.scrollIntoView({behavior: "smooth", block: "center"});


			setTimeout(() => {
				setHighlighted( false );
				setSelectedProject( "" );
			}, 2000)
		}
	}, [project, selectedProject, setSelectedProject]);*/

	return (
		<Card 
		//maxWidth="80ch" 
		borderRadius="xl" 
		variant="elevated" 
		overflow="clip" 
		id={project.uid} 
		transition="all 0.3s ease" 
		width="100%"
		position="relative"
		//boxShadow={highlighted ? "0px 0px 2em var(--chakra-colors-catppuccin-teal-300)" : ""}
		//zIndex={highlighted ? "8989898999999" : undefined}
		>
		{/*<Tag colorScheme="catppuccin.yellow" position="absolute" top="1em" right="1em" variant="solid">
				<TagLabel fontWeight="bold">TEST</TagLabel>
			</Tag>*/}

			<CardHeader>
				<Heading as="h3" fontSize="xx-large" color="catppuccin.text" marginBottom="5px">{project.title}</Heading>
				<Flex wrap="wrap" gap="5px">{project.techs.map((tech, i) => <SingleTech tech={tech} key={i}/>)}</Flex>
			</CardHeader>
			<CardBody>
				<Text>{project.description}</Text>
			</CardBody>

			<CardFooter paddingTop="0">
				<HStack spacing="0.5em" justifyContent="flex-end" width="100%">
					<ProjectRepoButton url={project.repoUrl}/>
					{project.live && (
						<ProjectActionButton text="See it live" url={project.live.url} icon={project.live.icon} colorScheme={project.live?.colorScheme || "catppuccin.mauve"}/>
					)}
				</HStack>
			</CardFooter>

			<BottomLine colors={project.techs.map(tech => `${tech.colorScheme}.100`)} height="4px"/>
		</Card>
	);
}


function ProjectColumns({projects, mobile}: {projects: ProjectType[], mobile: boolean}) {
	const nbColumns = useBreakpointValue({base: 1, md: 2, lg: 1, xl: 2, '2xl': 2}, {fallback: mobile ? 'base' : 'lg'}) ?? 1; 

	const parts: ProjectType[][] = [];
	for(let i = 0; i < nbColumns; i++) {
		parts.push([]);
	}

	projects.forEach((project, i) => parts[i % nbColumns].push(project));

	return (
		<Box paddingBottom="4em" width="100%">
			<HStack alignItems="flex-start" width="100%">
				{parts.map((part, i) => (
					<VStack spacing="1em" key={i} width="100%" flex={1}>
					{part.map((project, j) => (
						<NewProject project={project} key={j}/>
					))}
				</VStack>
				))}
			</HStack>
		

		<Center marginTop="2em">
			<Card variant="outline" borderRadius="xl" width={{base: "80%", lg: "50%"}}>
				<Center padding="3em">
					<VStack>
						<Icon as={FaPlus} boxSize="12" color="catppuccin.surface2"/>
						<Text color="catppuccin.surface2">And more coming</Text>
					</VStack>
				</Center>
			</Card>
		</Center>
	</Box>
	);
}

function Jobo() {
	if(Me.job !== undefined) {
		return (
			<Center width="min(36em, 90%)">
				<Card align='center' borderWidth="1px" borderColor="catppuccin.teal.100" borderRadius="xl">
					<CardHeader>
						<Heading size='md'>{Me.job.title}</Heading>
					</CardHeader>
					<CardBody>
						<Text>{Me.job.description}</Text>
					</CardBody>
					<CardFooter>
						<Button
						variant="solid"
						colorScheme='catppuccin.teal'
						leftIcon={<Icon as={BiMailSend} boxSize="6"/>}
						borderRadius="full" 
						size={{base: "lg", md: "md"}}
						as="a"
						href={"mailto:" + Me.email}
						>
							<Text>Send me an e-mail</Text>
						</Button>
					</CardFooter>
				</Card>
			</Center>
		);
	}
	else {
		return (<></>);
	}
}

//const selectedProjectContext = React.createContext<{selectedProject: string, setSelectedProject: (str: string) => void}>({selectedProject: "", setSelectedProject: () => {}});



function ScrollablePageContent({children}: {children: ReactNode}) {
	return (
		<Box 
		width="100%" 
		height="calc(100% - 2em - 10px)" 
		marginTop="calc(2em + 10px)" 
		overflowY={{base: "visible", lg: "scroll"}} 
		>
			{children}
		</Box>
	);
}



export default function TestPage({mobile, day}: {mobile: boolean, day: string}) {
	//const [selectedProject, setSelectedProject] = useState("");
	const isVerticalUI = useBreakpointValue({base: true, lg: false}, {fallback: mobile ? 'base' : 'lg'}) ?? (mobile ? true : false);

	return (
			<Flex 
			direction="column" 
			height="calc(100vh - 1em)" 
			width="100%" 
			marginTop="1em" 
			alignItems="center"
			>
				<Box
				position="absolute"
				backgroundColor="catppuccin.mantle"
				borderBottom="2px"
				borderColor="catppuccin.surface0"
				zIndex="-420"
				width="100%"
				top="0"
				height="3em"
				/>

				<Flex 
				height="100%" 
				maxWidth="1920px" 
				maxHeight="100vh" 
				direction={{base: "column", lg: "row"}} 
				alignItems={{base: "center", lg: "flex-start"}} 
				width="100%"
				paddingEnd={{base: 0, lg: "1em"}}
				>
					<VStack divider={<Divider orientation="vertical"/>} flex={1} width="100%" maxWidth="80ch" paddingX="1em">
						<AboutMe/>
						<CommitsGraph day={day} mobile={mobile}/>
						<Jobo/>
					</VStack>

					<ScrollablePageContent>
						<ProjectColumns projects={Object.values(allProjects.all)} mobile={mobile}/>
					</ScrollablePageContent>				
				</Flex>


		</Flex>
	);
}






export async function getServerSideProps<GetServerSideProps>(context: any) {

	const mobile = isMobile({ua: context.req.headers['user-agent']});

	// wow I sure do love localisation-related bugs
	// I hope I get more !!!!!
	// I have been a good bing
	const day = dayjs().format("YYYY-MM-DD");

	// I think these are good values,
	// it should always be almost up to date.
	//
	// stale :	10 minutes (10 * 60)
	// SWR :		30 minutes (30 * 60)
	context.res.setHeader(
		'Cache-Control',
		'public, s-maxage=600, stale-while-revalidate=1800'
	)


return {
	props: {
		mobile: mobile,
	},
}
}

