
import { tagAnatomy } from '@chakra-ui/anatomy';
import { createMultiStyleConfigHelpers } from '@chakra-ui/react';

const { definePartsStyle, defineMultiStyleConfig } = createMultiStyleConfigHelpers(tagAnatomy.keys);

const baseStyle = definePartsStyle({
  // define the part you're going to style
  container: {
    bg: 'catppuccin.surface1',
		_dark: {
			bg: 'catppuccin.surface1',
		}
  },
	label: {
    color: 'catppuccin.text',
	}
})

export const tagTheme = defineMultiStyleConfig({ variants: { catppuccin: baseStyle } })
