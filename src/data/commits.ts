
import * as cheerio from 'cheerio';
import dayjs from 'dayjs';

import iutcommits from "src/data/iutcommits.json";

export interface datedCommits {
	[key: string]: number;
}

async function getGithubCommits(year: number): Promise<datedCommits> {
	// So, I observed that the github profile page fetches
	// a page called "user/x/contributions", and I saw that
	// it was a dumb little page I could easily parse.
	// So that's what I did.
	const res = await fetch(`https://github.com/users/GuilianCD/contributions?to=${year}-12-31`);
	const body = await res.text();

	const $ = cheerio.load(body);

	const pageTitle = $(".js-yearly-contributions > div > h2").text();

	if(pageTitle.includes("contributions")) {
		const nbContribsTotal = parseInt(pageTitle.split(" ")[0]);

		if(nbContribsTotal === 0) {
			return {};
		}

		const result: {[key: string]: number} = {};
		//I'm pretty sure that data-level is not 0 if there's more than a contribution.
		const allContribsDays = $("*[data-level]:not([data-level='0'])");

		for (let i = 0; i < allContribsDays.length; i++) {
			const day = $(allContribsDays.get(i));

			const nbContribs = parseInt(day.text().split(" ")[0]);
			// shouldn't happen, but just in case
			const date = day.attr("data-date");
			if(date) {
				result[date] = nbContribs;
	}

		}

		return result;
	}

	return {};
}

// @TODO(guilian): gitlab's "calendar.json" doesn't seem to give me everything
async function getGitlabCommits(): Promise<datedCommits> {
	const res = await fetch("https://gitlab.com/users/GuilianCD/calendar.json");
	return await res.json();
}

async function getIutCommits(): Promise<datedCommits> {
	return iutcommits;
}





export interface oneCommitData {
	gitlab: number;
	github: number;
	iut: number;
	all: number;
}

export interface allCommitsData {
	total: oneCommitData;
	commits: {
		[key: string]: oneCommitData
	}
}

export default async function getAllCommits(): Promise<allCommitsData> {
	const thisYear = dayjs().year();

	// Gitlab shows the last 12 months 
	// Github shows for each year
	// I'm converting github to the last 12 months format
	const gitlab = await getGitlabCommits();
	const github = {
		...(await getGithubCommits(thisYear)),
		...(await getGithubCommits(thisYear - 1))
	}
	const iut = await getIutCommits();

	
	// @TODO(guilian) optimise this
	const allKeys = new Set([...Object.keys(github), ...Object.keys(gitlab), ...Object.keys(iut)]);

	const result: {[key: string]: oneCommitData} = {};

	const sums = {
		github: 0,
		gitlab: 0,
		iut: 0
	}

	allKeys.forEach(key => {
		if(dayjs(key).isBefore(dayjs().subtract(1, "year"))) {
			return;
		}

		let gHcommits = 0;
		let gLcommits = 0;
		let iutCommits = 0;

		if(Object.hasOwn(github, key)) {
			gHcommits += github[key];
		}
		if(Object.hasOwn(gitlab, key)) {
			gLcommits += gitlab[key];
		}
		if(Object.hasOwn(iut, key)) {
			iutCommits += iut[key];
		}

		sums.github += gHcommits;
		sums.gitlab += gLcommits;
		sums.iut		+= iutCommits;

		result[key] = {
			github: gHcommits,
			gitlab: gLcommits,
			iut: iutCommits,
			all: gHcommits + gLcommits + iutCommits
		}
	});

	return {
		total: {
			all: sums.github + sums.gitlab + sums.iut,
			github: sums.github,
			gitlab: sums.gitlab,
			iut: sums.iut
		},
		commits: result
	}
}

